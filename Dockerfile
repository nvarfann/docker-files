FROM php:7.2-fpm-alpine

RUN apk add bash
RUN apk add curl

RUN apk update && apk add build-base

RUN apk add postgresql postgresql-dev \
  && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
  && docker-php-ext-install pdo pdo_pgsql pgsql

RUN apk add zlib-dev git zip \
  && docker-php-ext-install zip

RUN curl -sS https://getcomposer.org/installer | php \
        && mv composer.phar /usr/local/bin/ \
        && ln -s /usr/local/bin/composer.phar /usr/local/bin/composer

COPY \www /var/www/html 
WORKDIR /var/www/html

RUN composer install --prefer-source --no-interaction
RUN composer create-project symfony/framework-standard-edition /var/www/html/test
#ENV PATH="~c:\work\.composer\vendor\bin:./vendor/bin:${PATH}"